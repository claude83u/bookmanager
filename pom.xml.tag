<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.tncy.claude83u.bookmanager</groupId>
    <artifactId>bookmanager</artifactId>
    <version>1.0.0</version>
    <name>BookManager</name>
    <url>http://mvn.tncy.eu/sites/demo-validation</url>
    <packaging>pom</packaging>
    
    <parent>
        <artifactId>tncy</artifactId>
        <groupId>net.tncy</groupId>
        <version>2019.1</version>
    </parent>
    
    <modules>
        <module>bookmanager-data</module>
        <module>bookmanager-service</module>
    </modules>
    
    <scm>
        <connection>scm:git:https://bitbucket.org/claude83u/bookmanager/</connection>
        <developerConnection>scm:git:https://bitbucket.org/claude83u/bookmanager/</developerConnection>
        <url>https://bitbucket.org/claude83u/bookmanager/</url>
        <tag>bookmanager-1.0.0</tag>
    </scm>
</project>
